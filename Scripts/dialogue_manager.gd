extends Node

@export_file var file_string : String

# Called when the node enters the scene tree for the first time.
func _ready():
	var file = FileAccess.open(file_string, FileAccess.READ)
	var text_table = JSON.parse_string(file.get_as_text())
	file.close()
	
	print(text_table["test1"])


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
